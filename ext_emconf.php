<?php
$EM_CONF[$_EXTKEY] = array(
    'title' => 'Publish Connector',
    'description' => 'CM City Media Publish Connector',
    'category' => 'fe',
    'shy' => 0,
    'version' => '11.5.0',
    'dependencies' => '',
    'conflicts' => '',
    'priority' => '',
    'loadOrder' => '',
    'module' => '',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'modify_tables' => '',
    'clearcacheonload' => 0,
    'lockType' => '',
    'author' => 'Kevin Andrews',
    'author_email' => 'kevin.andrews@cmcitymedia.de',
    'author_company' => 'CM City Media',
    'CGLcompliance' => '',
    'CGLcompliance_note' => '',
    'constraints' => array(
        'depends' => array(
            'typo3' => '9.5.0-11.99.99'
        ),
        'conflicts' => array(),
        'suggests' => array(),
    ),
    'suggests' => array(),
    'autoload' => array(
        'psr-4' => array('CMC\\' => 'Classes'),
        'classmap' => array('Classes'),
    ),
);
