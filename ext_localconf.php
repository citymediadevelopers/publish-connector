<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPItoST43('komm_publishconnector', '', '_pi1', 'list_type', FALSE);
$overrideSetup = 'plugin.tx_kommpublishconnector_pi1.userFunc = CMC\PublishConnector->main';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript('tx_kommpublishconnector', 'setup', $overrideSetup);

if (!is_array($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['publishconnector_cache'])) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['publishconnector_cache'] = [];
}