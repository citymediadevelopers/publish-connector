<?php

/*********************************************************************************
*
*	Serialisierungsklasse für Newsbereiche
*
*	V 1.0.3 - 09.09.2005
*
*	(c) cm city media GmbH - www.cmcitymedia.de
*	info@cmcitymedia.de
*
***********************************************************************************/

namespace CMC;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class PublishExchangeClientClass {
	
	
	/**
	*	Host
	*
	*	@access private
	*	@type string
	*/
	var $host = "134.60.71.208";
	
	/**
	*	Port
	*
	*	@access private
	*	@type int
	*/
	var $port = 82;
	
	/**
	*	Pfad
	*
	*	@access private
	*	@type string
	*/
	var $path;


	/**
	*	Link auf die eigene Seite(für RedDot und Typo3)
	*	@access private
	*	@type string 
	*/
	var $link;
	
	
	/**
	*	Setzt den Pfadnamen
	*
	*	@access public
	*	@param string Pfadnamen
	*/
	function setPathname($name) {
		$this->path = $name;
	}
	
	/**
	*	Setze Port
	*
	*	@access public
	*	@param int
	*/
	function setPort($port) {
		$this->port = $port;	
	}
	
	/**
	*	Setze Server
	*
	*	@access public
	*	@param int
	*/
	function setHost($host) {
		$this->host = $host;	
	}	



	/**
	*	Setzt den Link
	*
	*	@access public
	*	@param string Pfadnamen
	*/
	function setLink($link) {
		$this->link = $link;
	}

	
	/**
	*	Daten per Post �bermitteln
	*
	*	@access private
	*	@param string Hostname
	*	@param string Pfad
	*	@param string Referer
	*	@param string Serialisierte Daten
	*	@return string Fertig gerendertes HTML Dokument
	*/
	function postToHost() {
			
		// GET POST entspr. Methode und SESSION serialisieren
		// Session fuer Captcha Gaestebuch
		if (isset($_SESSION['captcha_spam'])) {
			$_POST["publish"]["session"] = $_SESSION;
		}
		
		
		$get = $this->codeArray($_GET, "enc");
		$post = $this->codeArray($_POST, "enc");
		$fileArray = $_FILES;
		$method = $_SERVER['REQUEST_METHOD'];
		$remoteAddr = $_SERVER['REMOTE_ADDR'];
		$session = $_REQUEST["PHPSESSID"];
		$host = $_SERVER["HTTP_HOST"];
		
		
		$referer = $this->link;
		
	
		/*
		Fuer Bild aus Gaestebuch II
		*/	
		$fileContent = "";
		if (is_array($_FILES)) {
			// Bilder laden
			foreach ($_FILES as $fileArray) {
				if (is_file($fileArray["tmp_name"])) {
					$fh = fopen($fileArray["tmp_name"], "rb");
					$fileContent = fread ($fh, filesize($fileArray["tmp_name"]));
					fclose ($fh);
				}
			}
			$fileContent = base64_encode($fileContent);
		}
		

		$url = "https://" . $this->host . $this->path;
		$data_to_send = "out=".urlencode(wddx_serialize_vars("get", "post", "fileArray", "fileContent", "method", "remoteAddr", "session", "referer", $host));
		print_r($fileContent);

		//if this is a get request check cache for page and serve if available
		if($method == 'GET') {
			$cache = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Cache\CacheManager::class)->getCache('publishconnector_cache');
			$cacheIdentifier = sha1($url. wddx_serialize_vars("get", "post", "fileArray", "fileContent", "method", "session", "referer", $host));
			$cachedResult = $cache->get($cacheIdentifier);
			if($cachedResult)
			{
				return $cachedResult;
			}				
		}

		// POST Request zum Zielserver aufbauen und Seriealisierte Daten versenden
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_to_send);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Host: ' . $this->host, 
			'Referer: ' . $this->link,
			'Content-type: application/x-www-form-urlencoded'
		));
		curl_setopt($ch, CURLOPT_SSL_CIPHER_LIST, 'DEFAULT@SECLEVEL=0');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		$liveResult = curl_exec($ch);

		if(curl_errno($ch))
		{
			$liveResult = "<body><div style='padding: 10px; border: 1px dotted grey; background-color:#f2dcdc'><h2 style='color: red; font-size: 16px; margin-bottom: 10px;'>Verbindungsproblem</h2></div></body>";
		} else{
			if($method == 'GET') {
				$cache->set($cacheIdentifier, $liveResult, [], 60);
			}
		}

		curl_close($ch);

		return $liveResult;
		
		// $fp = fsockopen($this->host, $this->port);
		// if ($fp) {
		// 	fputs($fp, "POST ".$this->path ." HTTP/1.0\r\n");
		// 	fputs($fp, "Host: ".$this->host."\r\n");
		// 	//fputs($fp, "Referer: ".$_SERVER['PHP_SELF']."\r\n");
		// 	fputs($fp, "Referer: ".$this->link."\r\n");
		// 	fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
		// 	fputs($fp, "Content-length: ". strlen($data_to_send) ."\r\n");
		// 	fputs($fp, "Connection: close\r\n\r\n");
		// 	fputs($fp, $data_to_send);
		// 	while(!feof($fp)) {
		// 		$liveResult .= fgets($fp);
		// 	}
		// 	fclose($fp);
		// }

		// return $liveResult;
	}
	

	/**
	*	Holt die fertige Seite 
	*
	*	@access public
	*	@param boolean BODY zurueckgeben true/false
	*	@return string
	*/
	function getPage($getBody = true) {
		// jh Mod
		// return '<p style="background-color: #ff0000; color: #ffffff">Publish System Content goes here!</p>';
		$out =  $this->postToHost();
		if ($getBody) {
			preg_match_all("=<body[^>]*>(.*)</body>=siU", $out, $a);
			
			if (!$a[1][0]) {
				preg_match_all("=<body[^>]*>(.*)</body>=si", $out, $a);
			}
			
			return $a[1][0];
		} else {
			return $out;
			//return utf8_encode($out);
		}
	}
	
	
	function codeArray($tmpArray, $coding) {
		if (is_array($tmpArray)) {
			foreach ($tmpArray as $key => $item) {
				if (is_array($item)) {
					$tmp[$key] = $this->codeArray($item, $coding);
				} else {
					if ($coding == "enc") {
						if ($item == "<" || $item == "<<") {
							$tmp[$key] = base64_encode($item);
						} else {
							$tmp[$key] = base64_encode(strip_tags ($item));
						}
					} elseif ($coding == "dec") {
						$tmp[$key] = base64_decode($item);
					}
				}
			}
		} else {
			$tmp = array();
		}
		return $tmp;
	}
	
	
}

//*********************************************************************************
?>
