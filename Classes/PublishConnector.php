<?php

namespace CMC;

/***************************************************************
*  Copyright notice
*
*  (c) 2009 <info@cmcitymedia.de>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 * Hint: use extdeveval to insert/update function index above.
 */

use DOMDocument;
use TYPO3\CMS\Core\MetaTag\MetaTagManagerRegistry;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Plugin 'PublishSystem Connector' for the 'komm_publishconnector' extension.
 *
 * @author	<info@cmcitymedia.de>
 * @package	TYPO3
 * @subpackage	tx_komm_publishconnector
 */
class PublishConnector extends \TYPO3\CMS\Frontend\Plugin\AbstractPlugin {
	var $prefixId      = 'tx_kommpublishconnector_pi1';		// Same as class name
	var $scriptRelPath = 'Classes/PublishConnector.php';	// Path to this script relative to the extension dir.
	var $extKey        = 'komm_publishconnector';	// The extension key.
	
	/**
	 * The main method of the PlugIn
	 *
	 * @param	string		$content: The PlugIn content
	 * @param	array		$conf: The PlugIn configuration
	 * @return	The content that is displayed on the website
	 */
	function main($content, $conf) {
		$this->conf = $conf;
		$this->pi_setPiVarDefaults();
		$this->pi_loadLL();
		$this->pi_USER_INT_obj = 1;	// Configuring so caching is not expected. This value means that no cHash params are ever set. We do this, because it's a USER_INT object!
		
		if(isset($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['komm_publishconnector']))
		{
			$this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['komm_publishconnector']);
		}
		else
		{
			$this->extConf = $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['komm_publishconnector'];
		}
		
		$path = $this->conf["path"];
		$module = stripslashes($this->cObj->data['tx_kommpublishconnector_scriptname']);
		$params = stripslashes($this->cObj->data['tx_kommpublishconnector_params']);
		$link = stripslashes($this->cObj->data['tx_kommpublishconnector_link']);
		
		// if (is_object($GLOBALS['BE_USER']) && $GLOBALS['BE_USER']->user['username']) {
		// 	$content = "<div style='padding: 10px; border: 1px dotted grey; background-color:#f2dcdc; margin-top: 15px' class='publishSystemNotify'>
		// 							<h2 style='color: red; font-size: 16px; margin-bottom: 10px;'>PublishSystem Modul:</h2>Dieses Modul wird von der cm city media GmbH verwaltet. Bitte nicht l&ouml;schen.</div>";
		// }

		if ($path && $module) {
			// Klasse Includieren
			$_GET["publish"]["p"] = $this->cObj->data["pid"];
			$server = new PublishExchangeClientClass();
			
			
			$server->host = trim($this->extConf['publishSystemExchangeUrl']);
			$server->port = trim($this->extConf['publishSystemExchangePort']);
			
			
			if (!$path) {
				$path = "";
			} else {
				$path .="/";
			}
			if (!$module) {
				$module = "error.htm";
			} else {
				$module .= ".php";
			}
			
			if ($params) {
				$module .= "?param=".$params;
			}
			$server->setPathname("/".$path.$module);
			if ($conf['typeNum']) {
				$typNum = '.'.$conf['typeNum'];
			}
			
			if ($link) {
					$id = $link;
			} else {
					$id = $GLOBALS['TSFE']->id.$typNum;
			}

			$server->setLink("index.php?id=".$id);
			$content .= $server->getPage();

			if (substr($module, 0, 4) == "news") {
                $this->addShareMetaTags($content, $id);
            }
		} else {
			$error = "Unspecified Error";
			if (!$path) {
				$error .= "<li>Es ist im TypoScript noch kein Pfad angegeben worden (Bsp: plugin.tx_kommpublishconnector_pi1.path= pluederhausen)</li>";
			}
			if (!$module) {
				$error .= "<li style='margin-top: 5px'>Es ist noch kein Modul angegeben worden</li>";
			}
			$content .= "<div style='padding: 10px; border: 1px dotted grey; background-color:#f2dcdc'>
									<h2 style='color: red; font-size: 16px; margin-bottom: 10px;'>PublishSystem Connector Fehler:</h2><ul>$error</ul></div>";
		}

			
		/**
		 * E-Mail-Adressen in $content mit der TYPO3-eigenen Funktion verschl�sseln
		 * Start: <a href="mailto:waldshut-tiengen@alba.info">waldshut-tiengen@alba.info</a>
		 * Ziel: <a href="javascript:linkTo_UnCryptMailto('ocknvq,oczBowuvgtocpp0fg');" class="mail">waldshut-tiengen(@)alba.info</a>
		 * Ersetzungszeichen: $GLOBALS['TSFE']->tmpl->setup['page.']['config.']['spamProtectEmailAddresses_atSubst']
		 */
		// pr�fen ob E-Mail-Adressen verschl�sselt werden sollen. 
		// siehe page.config im Root-TS
		if ($GLOBALS['TSFE']->tmpl->setup['page.']['config.']['spamProtectEmailAddresses']){
		
			// alle mailto-Anchor aus content einlesen
			$pattern = '`\<a([^>]+)href\=\"mailto\:([^">]+)\"([^>]*)\>(.*?)\<\/a\>`ism';
			preg_match_all($pattern, $content, $matches);
			
			// ersetzen
			foreach ($matches[0] as $key=>$value){
				$encryptedArray = $this->cObj->getMailTo($matches[2][$key], $matches[4][$key]);
				$replacement = '<a href="'.$encryptedArray[0].'" class="mail">'.$encryptedArray[1].'</a>';
				$content = str_replace( $value, $replacement, $content );
			}
			
		}
	
		$content = str_replace('?&', '&', $content);
		$content = str_replace('.php&', '.php?', $content);

		return $this->pi_wrapInBaseClass($content);
	}

	function addShareMetaTags($content, $id) {
        // Represent content as DOMDocument to easily access the meta tag information
        $dom = new DOMDocument();
        @$dom->loadHTML($content);

        $base_url = "https://".$_SERVER["HTTP_HOST"]."/index.php?id=".$id."&publish[id]=";
        $share_id = $dom->getElementById("og:url");
        $share_title = $dom->getElementById("og:title");
        $share_type = $dom->getElementById("og:type");
        $share_description = $dom->getElementById("og:description");
        $share_image = $dom->getElementById("og:image");

        // If the above DOM elements do not exists, then the news list was called. Here we don't add the tags to the DOM.
        if ($share_id != null && $share_title != null && $share_type != null && $share_description != null && $share_image != null) {
            $share_id = $share_id->getAttribute("content");
            $share_title = $share_title->getAttribute("content");
            $share_type = $share_type->getAttribute("content");
            $share_description = $share_description->getAttribute("content");
            $share_image = $share_image->getAttribute("content");

            $metaTagManager = GeneralUtility::makeInstance(MetaTagManagerRegistry::class)->getManagerForProperty('og:url');
            $metaTagManager->addProperty('og:url', $base_url.$share_id);
            $metaTagManager = GeneralUtility::makeInstance(MetaTagManagerRegistry::class)->getManagerForProperty('og:title');
            $metaTagManager->addProperty('og:title', $share_title);
            $metaTagManager = GeneralUtility::makeInstance(MetaTagManagerRegistry::class)->getManagerForProperty('og:type');
            $metaTagManager->addProperty('og:type', $share_type);
            $metaTagManager = GeneralUtility::makeInstance(MetaTagManagerRegistry::class)->getManagerForProperty('og:description');
            $metaTagManager->addProperty('og:description', $share_description);
            $metaTagManager = GeneralUtility::makeInstance(MetaTagManagerRegistry::class)->getManagerForProperty('og:image');
            $metaTagManager->addProperty('og:image', $share_image);
        }
    }
}
