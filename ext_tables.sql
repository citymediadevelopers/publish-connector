#
# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
    tx_kommpublishconnector_scriptname tinytext,
    tx_kommpublishconnector_params tinytext,
    tx_kommpublishconnector_link text
);