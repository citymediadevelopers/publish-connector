<?php

$tempColumns = array (
    'tx_kommpublishconnector_scriptname' => array (
        'exclude' => 1,
        'label' => 'Script Name',
        'config' => array (
            'type' => 'input',
            'size' => '30',
        )
    ),
    'tx_kommpublishconnector_params' => array (
        'exclude' => 1,
        'label' => 'Parameter',
        'config' => array (
            'type' => 'input',
            'size' => '30',
        )
    ),
    'tx_kommpublishconnector_link' => array (
        'exclude' => 1,
        'label' => 'Link',
        'config' => array (
            'type' => 'group',
            'internal_type' => 'db',
            'allowed' => 'pages',
            'size' => 1,
            'minitems' => 0,
            'maxitems' => 1,
        )
    ),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'tt_content',
    $tempColumns
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'tt_content',
    "tx_kommpublishconnector_scriptname,tx_kommpublishconnector_params,tx_kommpublishconnector_link"
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['komm_publishconnector_pi1']='layout,select_key';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    array(
        'Publish Connector',
        'komm_publishconnector_pi1',
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath("komm_publishconnector") . 'ext_icon.png'
    ),
    'list_type',
    "komm_publishconnector"
);
